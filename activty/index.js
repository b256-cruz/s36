// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoutes.js")

// Server setup
const app = express();
const port = 4000;

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@b256-cruz.rvtoo2s.mongodb.net/B256_to-do?retryWrites=true&w=majority",{
    useNewUrlParser: true,
	useUnifiedTopology: true
}).then(() => {
    console.log('Were connected to the cloud database');
}).catch((err) => {
    console.error('Error connecting to MongoDB:', err.message);
});

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}))

// Add the task route
app.use("/task", taskRoute)

// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`));

