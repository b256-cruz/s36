const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskControllers.js");


router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => {res.send(resultFromController)});
});

router.post("/create", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => {res.send(resultFromController)});
});

router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => {res.send(resultFromController)});
})

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => {res.send(resultFromController)});
})

router.put('/:id/complete', taskController.completeTask);

module.exports = router;