const Task = require("../models/Task.js");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

module.exports.createTask = (requestBody) => {
	return Task.findOne({name: requestBody.name}).then((result, error) => {
		if(result !== null && result.name == requestBody.name) {
			return 'Duplicate Task Found';
		} else {
			let newTask = new Task({
				name: requestBody.name
			})
			return newTask.save().then((savedTask, savedErr) => {
				if(savedErr) {
					console.log(savedErr);
					return 'Task Creation Failed';
				} else {
					return savedTask;
				} 
			})
		}
	})
}

module.exports.deleteTask = (paramsId) => {
	return Task.findByIdAndRemove(paramsId).then((removeTask, err) => {
		if(err) {
			console.log(err)
			return 'Task was not removed';
		} else {
			return 'Task successfully removed';
		}
	})
}

module.exports.updateTask = (paramsId, requestBody) => {	
	return Task.findById(paramsId).then((result, err) => {
		if(err) {
			console.log(error);
			return 'Error Found';
		} else {			
			result.name = requestBody.name
			return result.save().then((updatedTask, err) => {
				if(err) {
					console.log(err);
					return false;
				} else {
					return updatedTask;
				}
			})
		}
	})
}

module.exports.completeTask = (req, res) => {
	Task.findByIdAndUpdate(req.params.id, { status: 'complete' }, { new: true })
	.then((task) => {
		res.send(task);
	})
	.catch((err) => {
		res.status(500).send(err);
	});
};